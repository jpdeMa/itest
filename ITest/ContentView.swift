//
//  ContentView.swift
//  ITest
//
//  Created by Jean-Pierre on 29/03/2020.
//  Copyright © 2020 Jean-Pierre. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        HStack {
            Image(systemName: "pen.circle.fill")
                .font(.title)
            
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
